const yaml = require('js-yaml');
const postcss = require('postcss');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const execSync = require('child_process').execSync;

module.exports = function(eleventyConfig) {
  eleventyConfig.addDataExtension('yaml', contents => yaml.load(contents));

  eleventyConfig.addNunjucksAsyncFilter('postcss', function(cssCode, done) {
    postcss([tailwindcss(require('./tailwind.config.js')), autoprefixer()])
      .process(cssCode)
      .then(
        r => done(null, r.css),
        e => done(e, null)
      );
  });
  eleventyConfig.addWatchTarget('./src/styles/**/*.css');

  eleventyConfig.addPassthroughCopy('./static');

  eleventyConfig.addFilter("array", (object) => {
    if(Object.prototype.toString.call(object) === '[object Array]') {
      return object;
    } else if(typeof object === 'string') {
      return object.split(',');
    } else {
      return object;
    };
  });

  eleventyConfig.addFilter("rdoc", (api) => {
    return execSync(`ri --format rdoc --no-pager '${api}' | rdoc --pipe`);
  });

  eleventyConfig.addFilter("rubyapi", (api) => {
    return `https://rubyapi.org/o/${api.replace('#', '#method-i-').replace('::', '#method-c-')}`;
  });

  eleventyConfig.addFilter("code", (code) => {
    if(code.match(/\n|\r/g)) {
      return `<pre><code>${code}</code></pre>`;
    } else {
      return `<code>${code}</code>`;
    }
  });

  return {
    pathPrefix: '/ruby-on-shell/',
    dir: {
      input: 'src',
      output: 'dist'
    },
  };
};
