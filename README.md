<img width="100%" src="https://capsule-render.vercel.app/api?type=waving&section=header&color=DC2626&fontColor=FEF2F2&height=256&text=Ruby%20on%20Shell&desc=A%20handbook%20on%20using%20Ruby%20for%20shell%20scripting&fontAlignY=40" />

## 💡 About

**Ruby on Shell** is a handbook on using [Ruby](https://www.ruby-lang.org) for shell scripting. [Read it now!](https://nnb.codeberg.page/ruby-on-shell)

## 💌 Credits

Special thanks to:
- [**Idiosyncratic Ruby**](https://idiosyncratic-ruby.com) by [Jan Lelis](https://janlelis.com)
- [**Replacing Bash Scripting with Python**](https://github.com/ninjaaron/replacing-bash-scripting-with-python) by [Aaron Christianson](https://github.com/ninjaaron)
- [**Enhanced Shell Scripting with Ruby**](https://www.devdungeon.com/content/enhanced-shell-scripting-ruby) by [NanoDano](https://www.devdungeon.com/users/nanodano)
- [**Ruby API**](https://rubyapi.org) by [it's contributors](https://github.com/rubyapi/rubyapi/graphs/contributors)
- [**ManKier**](https://www.mankier.com) by it's developers


<a href="https://codeberg.org/NNB">
  <img
    width="100%"
    src="https://capsule-render.vercel.app/api?type=waving&section=footer&color=DC2626&fontColor=FEF2F2&height=128&desc=Made%20with%20%26lt;3%20by%20NNB&descAlignY=80"
    alt="Made with <3 by NNB"
  />
</a>
