const execSync = require('child_process').execSync;

module.exports = {
  date: new Date(),
  ruby_version: execSync('ruby -e "print RUBY_VERSION"'),
};
