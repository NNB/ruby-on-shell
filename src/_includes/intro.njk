{% macro sub_section(name) %}
  <div class="flex flex-col gap-4">
    <h3 class="text-2xl sm:text-3xl font-bold" id="{{ name | slugify }}">
      <a class="link link-hover" href="#{{ name | slugify }}">
        {{ name }}
      </a>
    </h3>

    {{ caller() }}
  </div>
{% endmacro %}

{% macro collapse(name, checked=false, style='') %}
  <div class="collapse collapse-arrow bg-base-200 rounded {{ style }}">
    <input type="checkbox" {% if checked %}checked{% endif %} />
    <div class="collapse-title flex items-center sm:text-xl">
      {{ name }}
    </div>
    <div class="collapse-content prose max-w-full min-w-0">
      {{ caller() }}
    </div>
  </div>
{% endmacro %}

<div class="px-6 py-8 flex flex-col gap-16 md:rounded-box bg-base-100" id="introduction">
  <h2 class="text-center text-3xl sm:text-4xl font-bold">
    <a class="link link-hover" href="#introduction">
      Introduction
    </a>
  </h2>

  <p>
    Welcome to Ruby on Shell! This is a guide/handbook on using Ruby for shell
    scripting. This handbook will list most shell commands, and it's Ruby equivalent.
    This page is using the Ruby's document for version {{ build.ruby_version }} as
    a referent. If you have questions or want to feedback, please
    <a
      class="link link-primary"
      href="https://codeberg.org/NNB/ruby-on-shell/issues/new"
      target="_blank"
      rel="noopener noreferrer"
    >open an issue</a>.
  </p>

  {% call sub_section('Why not Bash?') %}
    {% call collapse('Why try to reinvent bash?', checked=true) %}
      <p class="italic">
        Quote from
        <a
          class="link link-primary"
          href="https://github.com/ninjaaron/replacing-bash-scripting-with-python"
          target="_blank"
          rel="noopener noreferrer"
        >Closh's README</a>:
      </p>

      <ul>
        <li>
          Bash has obscure syntax for non-trivial operations and lots of WTF moments.
        </li>
        <li>
          It treats everything as text while we mostly need to manipulate structured
          information.
        </li>
        <li>
          It is a large codebase which makes it difficult to hack on it and try
          innovative ideas. Which is one of the reasons why the shell did not improve
          much in recent decades.
        </li>
      </ul>
    {% endcall %}

    {% call collapse('When to use Bash') %}
      <p class="italic">
        Quote from
        <a
          class="link link-primary"
          href="https://github.com/ninjaaron/replacing-bash-scripting-with-python"
          target="_blank"
          rel="noopener noreferrer"
        >Replacing Bash Scripting with Python</a>:
      </p>

      <p>
        I do believe that for a program which deals primarily with starting processes
        and connecting their inputs and outputs, as well as certain kinds of file
        management tasks, the shell should still be the first candidate. A good
        example might be setting up a server. I keep config files for my shell
        environment in Git (like any sane person), and I use <code>sh</code> for
        all the setup. That's fine. In fact, it's great. Running some commands and
        symlinking files is a usecase that fits perfectly to the strengths of the
        shell.
      </p>

      <p>
        I also have shell scripts for automating certain parts of my build, testing
        and publishing workflow for my programming, and I will probably continue
        to use such scripts for a long time.
      </p>

      {% call collapse('Warning Signs', style='border border-base-100') %}
        <p>
          Many people have rule about the length of their Bash scripts. It is oft
          repeated on the Internet that, "If your shell script gets to fifty lines,
          rewrite in another language," or something similar. The number of lines
          varies from 10 to 20 to 50 to 100. Among the Unix old guard, "another
          language" is basically always Perl.
        </p>

        <p>
          This kind of rule isn't too bad. Length isn't the problem, but length
          <em>can</em> be a side-effect of complexity, and complexity is sort of
          the arch-enemy of Bash. I look for the use of certain features to be an
          indicator that it's time to consider a rewrite. (note that "rewrite" can
          mean moving certain parts of the logic into another language while still
          doing orchestration in Bash). These "warning signs are" listed in order
          of more to less serious.
        </p>

        <ul>
          <li>
            If you ever need to type the characters <code>IFS=</code>, rewrite
            immediately. You're on the highway to Hell.
          </li>
          <li>
            If data is being stored in Bash arrays, either refactor so the data
            can be streamed through pipelines or use a different language. As with
            <code>IFS</code>, it means you're entering the wild world of the
            shell's string splitting rules. That's not the world for you.
          </li>
          <li>
            If you find yourself using braced parameter expansion syntax,
            <code>${my_var}</code>, and anything is between those braces besides
            the name of your variable, it's a bad sign. For one, it means you might
            be using an array, and that's not good. If you're not using an array,
            it means you're using the shell's string manipulation capabilities.
            There are cases where this might be allowable (determining the basename
            of a file, for example), but the syntax for that kind of thing is very
            strange, and so many other languages supply better string manipulating
            tools.
          </li>
          <li>
            Dealing with process output in a loop is not a great idea. If you HAVE
            to do it, the only right way is with <code>while IFS= read -r line</code>.
            Don't listen to anyone who tells you differently, ever. Always try to
            refactor this case as a one-liner with AWK or Perl, or write a script
            in another language to process the data and call it from Bash.  If you
            have a loop like this, and you are starting any processes inside the
            loop, you will have major performance problems. This will eventually
            lead to refactoring with Bash built-ins. In the final stages, it
            results in madness and suicide.
          </li>
          <li>
            Bash functions, while occasionally useful, can be a sign of trouble.
            All the variables are global by default. It also means there is enough
            complexity that you can't do it with a completely linear control flow.
            That's also not a good sign for Bash. A few Bash functions might be
            alright, but it's a warning sign.
          </li>
          <li>
            Conditional logic, while it can definitely be useful, is also a sign
            of increasing complexity. As with functions, using it doesn't mean you
            have to rewrite, but every time you write one, you should ask yourself
            the question as to whether the task you're doing isn't better suited
            to another language.
          </li>
        </ul>

        <p>
          Finally, whenever you use a <code>$</code> in Bash (parameter expansion),
          you must use quotation marks. Always only ever use quotation marks.
          Never forget. Never be lazy. This is a security hazard. As previously
          mentioned, Bash is an injection honeypot. There are a few cases where
          you don't need the quotation marks. They are the exceptions. Do not
          learn them. Just use quotes all the time. It is always correct.
        </p>
      {% endcall %}
    {% endcall %}
  {% endcall %}

  {% call sub_section('Why Ruby?') %}
    {% call collapse('Roots of Rubyism', checked=true) %}
      <p class="italic">
        Quote from
        <a
          class="link link-primary"
          href="https://idiosyncratic-ruby.com/31-roots-of-rubyism.html"
          target="_blank"
          rel="noopener noreferrer"
        >Idiosyncratic Ruby</a>:
      </p>

      <p>
        It is a language with a <strong>terrific community</strong> (welcoming and
        always questioning itself) and <strong>ecosystem</strong> (a lot of problems
        already solved), which is <strong>beautiful</strong> (focus on productivity),
        a little <strong>conservative</strong> (in the sense that it is easy to
        work with), and <strong>general purpose</strong> (a good choice in most cases).
      </p>
    {% endcall %}

    <div class="flex flex-col gap-8">
      <div class="flex flex-col gap-4">
        <h3 class="text-xl font-bold">Familiarity:</h3>

        <p>
          The Ruby's syntax is extremely similar to the established shell scripting
          languages. From it's semi-whitespace-significant style to it's
          keyword-base blocks syntax...
        </p>

        <p>
          In Ruby everything is an object of some class, and every class have ton
          of powerful methods. Because of that, you could write a straightforward
          method chaining just like a pipeline in shell.
        </p>

        <div class="prose max-w-full min-w-0">
          <pre><code># Bash
echo 'zab rab oof' | rev | sed -e 's/bar/banana/g' | tr '[[:lower:]]' '[[:upper:]]' | cut -d ' ' -f 2  #=&gt; "BANANA"

# Ruby
'zab rab oof'.reverse.gsub('bar', 'banana').upcase.split[1]  #=&gt; "BANANA"</code></pre>
        </div>
      </div>

      <div class="flex flex-col gap-4">
        <h3 class="text-xl font-bold">Convenient:</h3>

        <p>
          Although Ruby in not a shell language,
          it can call shell command (with its syntax) inside Ruby just by wrapping it with backticks:
        </p>

        <div class="prose max-w-full min-w-0">
          <pre><code>`ls`  # List directory contents

bar = 'banana'
`echo foo #{bar}`  #=&gt; "foo banana\n"

`true &amp;&amp; echo '!dlroW ,olleH' | rev`  #=&gt; "Hello, World!\n"</code></pre>
        </div>

        <p>
          The more you understand Ruby, the shorter and nicer your code can be:
        </p>

        <div class="prose max-w-full min-w-0">
          <pre><code># Count lines of file:
File.open('path/to/file') { |file| file.read.split("\n").size }
File.open('path/to/file') { |file| file.read.lines.size }
File.open('path/to/file') { |file| file.readlines.size }
File.open('path/to/file') { _1.readlines.size }
File.read('path/to/file').lines.size
File.readlines('path/to/file').size

# Count the number of even bytes in a string:
'Hello, World!'.bytes.select { |number| number % 2 == 0 }.size
'Hello, World!'.bytes.select { |number| number.even? }.size
'Hello, World!'.bytes.select { _1.even? }.size
'Hello, World!'.bytes.select(&amp;:even?).size
'Hello, World!'.bytes.count(&amp;:even?)</code></pre>
        </div>
      </div>

      <div class="flex flex-col gap-4">
        <h3 class="text-xl font-bold">It's Fun:</h3>

        <p>
          Ruby is a joy to read and write with. The language is elegantly designed
          to make the script we write in feel natural.
        </p>

        <p>
          Ruby is also flexible, it gives you the freedom to write the code the
          way you want. E.g: Instead of the keyword-base blocks syntax you can
          write blocks in C-style <code>{ ... }</code>.
        </p>

        <p>
          The language is so expressive, it's perfect for
          <a
            class="link link-primary"
            href="https://code.golf/wiki/langs/ruby"
            target="_blank"
            rel="noopener noreferrer"
          >code-golfing</a>
          or do some crazy tricks like in the
          <a
            class="link link-primary"
            href="https://idiosyncratic-ruby.com/75-ruby-tricks-of-2018.html"
            target="_blank"
            rel="noopener noreferrer"
          >TRIC contest</a>.
        </p>
      </div>
    </div>
  {% endcall %}

  {% call sub_section('Getting started') %}
    <p>
      First, you need to learn the basic of Ruby. There are many ways to learn Ruby,
      but I found this tutorial to be the best one to introduce you to the languages:
    </p>

    {% call collapse('Ruby Programming in One Video') %}
      <iframe
        class="w-full aspect-video"
        src="https://y.com.sb/embed/8wZ2ZD--VTk"
        allowfullscreen
      ></iframe>
    {% endcall %}

    <p>
      If you prefer reading,
      <a
        class="link link-primary"
        href="https://www.theodinproject.com/paths/full-stack-ruby-on-rails/courses/ruby"
        target="_blank"
        rel="noopener noreferrer"
      >The Odin Project</a>
      is a great alternative.
    </p>

    <p>
      Or if you come from other programming languages,
      <a
        class="link link-primary"
        href="https://www.ruby-lang.org/en/documentation/ruby-from-other-languages"
        target="_blank"
        rel="noopener noreferrer"
      >Ruby From Other Languages</a>
      is the quickest path to learn Ruby.
    </p>

    <p>
      Next you can read
      <a
        class="link link-primary"
        href="https://www.devdungeon.com/content/enhanced-shell-scripting-ruby"
        target="_blank"
        rel="noopener noreferrer"
      >Enhanced Shell Scripting with Ruby</a>
      which is focused way more on using Ruby for shell scripting.
    </p>

    <p>
      The Ruby document is a very organized and easy to digest resource. You can
      easily go through all the basic objects and those are methods to utilize the
      most out of the language power:
    </p>

    <ul class="list-inside list-disc">
      <li><a class="link link-hover font-mono" href="https://rubyapi.org/o/string"     target="_blank" rel="noopener noreferrer">String</a></li>
      <li><a class="link link-hover font-mono" href="https://rubyapi.org/o/integer"    target="_blank" rel="noopener noreferrer">Integer</a></li>
      <li><a class="link link-hover font-mono" href="https://rubyapi.org/o/array"      target="_blank" rel="noopener noreferrer">Array</a></li>
      <li><a class="link link-hover font-mono" href="https://rubyapi.org/o/hash"       target="_blank" rel="noopener noreferrer">Hash</a></li>
      <li><a class="link link-hover font-mono" href="https://rubyapi.org/o/enumerable" target="_blank" rel="noopener noreferrer">Enumerable</a></li>
    </ul>

    <p>
      Some other useful resource:
      <a
        class="link link-primary"
        href="https://idiosyncratic-ruby.com/"
        target="_blank"
        rel="noopener noreferrer"
      >Idiosyncratic Ruby</a>,
      <a
        class="link link-primary"
        href="https://rubystyle.guide"
        target="_blank"
        rel="noopener noreferrer"
      >Ruby Style Guide</a>.
    </p>
  {% endcall %}

  {% call sub_section('Ruby as interactive shell') %}
    <p>
      Ruby is great for scripting but it's syntax is a bit strict for quick and frequent tasks.
      The best way to use Ruby in the command-line is to bind <code>irb</code> to a key like
      <kbd class="kbd">Alt</kbd> + <kbd class="kbd">R</kbd> for example,
      so you can quickly toggle between simple shell mode and Ruby scripting mode.
    </p>

    <p>Add the keybind to your prefer interactive shell:</p>

    <p>For Bash (<code>~/.bashrc</code>):</p>

    <div class="prose max-w-full min-w-0">
      <pre><code>bind -x '"\er":"irb"'</code></pre>
    </div>

    <p>For Zsh (<code>~/.zshrc</code>):</p>

    <div class="prose max-w-full min-w-0">
      <pre><code>bindkey -s "\er" '^Qirb^J'</code></pre>
    </div>

    <p>For Fish (<code>~/.config/fish/config.fish</code>):</p>

    <div class="prose max-w-full min-w-0">
      <pre><code>bind \er 'irb; commandline -f repaint'</code></pre>
    </div>
  {% endcall %}
</div>
