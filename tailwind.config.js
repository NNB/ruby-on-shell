module.exports = {
  content: ['./src/**/*.{njk,md}'],
  plugins: [require('daisyui'), require('@tailwindcss/typography')],
  daisyui: {
    themes: [
      {
        mytheme: {
          'primary': '#83a598',
          'primary-focus': '#458588',
          'primary-content': '#282828',
          'secondary': '#8ec07c',
          'secondary-focus': '#689d6a',
          'secondary-content': '#282828',
          'accent': '#d3869b',
          'accent-focus': '#b16286',
          'accent-content': '#282828',
          'neutral': '#32302f',
          'base-100': '#504945',
          'base-200': '#3c3836',
          'base-300': '#32302f',
          'base-content': '#ebdbb2',
          'info': '#83a598',
          'info-focus': '#458588',
          'info-content': '#282828',
          'success': '#b8bb26',
          'success-focus': '#98971a',
          'success-content': '#282828',
          'warning': '#fabd2f',
          'warning-focus': '#d79921',
          'warning-content': '#282828',
          'error': '#fb4934',
          'error-focus': '#cc241d',
          'error-content': '#282828'
        },
      },
    ],
  },
  theme: {
    fontFamily: {
      serif: [
        'Be Vietnam Pro',
        'ui-sans-serif',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      mono: [
        'JetBrains Mono',
        'ui-monospace',
        'SFMono-Regular',
        'Menlo',
        'Monaco',
        'Consolas',
        '"Liberation Mono"',
        '"Courier New"',
        'monospace'
      ]
    }
  }
};
